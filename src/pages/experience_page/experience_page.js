function fill_input_position(id){
    var exp_position = id.split("-")[1];
    var x = document.getElementById(id).value;
    document.getElementById("resume_position-" + exp_position).innerHTML = x;
    var txt = document.getElementById("resume_experience-" + exp_position);
    var y = document.getElementById("input_employer-" + exp_position).value;
    var z = document.getElementById('input_experience_description-' + exp_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(exp_position, x, y, z);
    sessionStorage.setItem("input_position-" + exp_position, x);
    if (validate_input(x) === 0){
        document.getElementById(id).style.border = '1px solid #98E37E';
        document.getElementById("exp_position_ok").style.visibility = 'visible';
        document.getElementById("exp_position_bad").style.visibility = 'hidden';
    } else {
        document.getElementById(id).style.border = '1px solid #EF5050';
        document.getElementById("exp_position_ok").style.visibility = 'hidden';
        document.getElementById("exp_position_bad").style.visibility = 'visible';
    }
}

function validate_input(text){
    if (text.length < 2){
        return -1;
    }
    return 0;
}
function fill_input_employer(id) {
    var exp_position = id.split("-")[1];
    var x = document.getElementById("input_employer-" + exp_position).value;
    document.getElementById("resume_employer-" + exp_position).innerHTML = x;
    var txt = document.getElementById("resume_experience-" + exp_position);
    var y = document.getElementById("input_employer-" + exp_position).value;
    var z = document.getElementById('input_experience_description-' + exp_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }

    check_filled_inputs_for_storage(exp_position, x,  y, z);
    sessionStorage.setItem("input_employer-" + exp_position, x);
    if (validate_input(x) === 0){
        document.getElementById(id).style.border = '1px solid #98E37E';
        document.getElementById("exp_employer_ok").style.visibility = 'visible';
        document.getElementById("exp_employer_bad").style.visibility = 'hidden';
    } else {
        document.getElementById(id).style.border = '1px solid #EF5050';
        document.getElementById("exp_employer_ok").style.visibility = 'hidden';
        document.getElementById("exp_employer_bad").style.visibility = 'visible';
    }
}
function  fill_input_experience_description(id) {
    var exp_position = id.split("-")[1];
    var x = document.getElementById("input_experience_description-" + exp_position).value;
    document.getElementById("resume_experience_description-" + exp_position).innerHTML = x;
    var txt = document.getElementById('resume_experience-' + exp_position);
    var y = document.getElementById("input_employer-" + exp_position).value;
    var z = document.getElementById('input_experience_description-' + exp_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(exp_position, x, y, z);
    sessionStorage.setItem("input_experience_description-" + exp_position, x);
    if (x.length > 0){
        document.getElementById(id).style.border = '1px solid #98E37E';
    } else {
        document.getElementById(id).style.border = '1px solid #EF5050';
    }
}
function check_filled_inputs_for_storage(exp_position, x, y ,z) {
    if(exp_position > 1 && (x !== '' || y !== '' || z !== '')) {
        sessionStorage.setItem("experience-" + exp_position, x);
    } else if (exp_position > 1 && x === '' && y === '' && z === '' && sessionStorage.getItem("experience-" + exp_position) != null) {
        sessionStorage.removeItem("experience-" + exp_position);
    }
}

function fill_input_experience_date_start(id) {
    var exp_position = id.split("-")[1];
    var x = document.getElementById("input_start_date-" + exp_position).value;
    document.getElementById("resume_experience_date_start-" + exp_position).innerHTML = x;
    sessionStorage.setItem("input_start_date-" + exp_position, x);
}

function fill_input_experience_date_end(id) {
    var exp_position = id.split("-")[1];
    var x = document.getElementById("input_end_date-" + exp_position).value;
    document.getElementById("resume_experience_date_end-" + exp_position).innerHTML = x;
    sessionStorage.setItem("input_end_date-" + exp_position, x);
}
function go_to_private_info_page() {
    window.location.href = "../private_info/private_info.html";
}
function go_to_education_page() {
    window.location.href = "../education_page/education.html";
}

function get_data_session() {
    $(window).on('load', function(){
        console.log("mopvedi");
        let count = 2;
        while (sessionStorage.getItem("experience-" + count) !== null) {
            add_new_experience(count);
            add_new_resume_experience(count);
            count++;
        }

        $("#experience").children().each(function () {
            const id = $(this).attr("id").split("-")[1];

            let input_position = sessionStorage.getItem("input_position-" + id);
            if (input_position !== null && input_position !== '') {
                $("#input_position-" + id).val(input_position);
            }

            let input_employer = sessionStorage.getItem("input_employer-" + id);
            if (input_employer !== null && input_employer !== '') {
                $("#input_employer-" + id).val(input_employer);
            }

            let input_exp_description = sessionStorage.getItem("input_experience_description-" + id);
            if(input_exp_description !== null && input_exp_description !=='') {
                $("#input_experience_description-" + id).val(input_exp_description);
            }

            let input_start_date = sessionStorage.getItem("input_start_date-" + id);
            if(input_start_date !== null && input_start_date !== '') {
                $("#input_start_date-" + id).val(input_start_date);
            }

            let input_end_date = sessionStorage.getItem("input_end_date-" + id);
            if(input_end_date !== null && input_end_date !== '') {
                $("#input_end_date-" + id).val(input_end_date);
            }
        });
    });
}

function load_resume_html() {
    $(function () {
        $("#resume").load("/src/pages/resume/resume.html");
    })
}

function load_new_experience() {
    $(function () {
        $("#button_more").click(function () {
            var experience = $("#experience").clone();
            var count = 1;
            experience.children().each(function () {
                    count++;
                }
            );
            add_new_experience(count);

            var resume = $("#resume_experience_div").clone();
            var resume_count = 1;
            resume.children().each(function () {
                resume_count++;
            });
            add_new_resume_experience(resume_count);
        });
    });
}

function add_new_experience(count) {
    let experience = $("#experience").clone();
    var exp = experience.find("#experience-1").attr("id", "experience-" + count);
    exp.find("#input_position-1").attr("id", "input_position-" + count)
        .attr("placeholder", "დეველოპერი, დიზაინერი და ა.შ").val(null);
    exp.find("#input_position_label-1").attr("id", "input_position_label-" + count).attr("for", "input_position-" + count);
    exp.find("#input_employer-1").attr("id", "input_employer-" + count)
        .attr("placeholder", "დამსაქმებელი").val(null);
    exp.find("#input_employer_label-1").attr("id", "input_employer_label-" + count).attr("for", "input_employer-" + count);
    exp.find("#input_start_date-1").attr("id", "input_start_date-" + count)
        .attr("placeholder", "mm/dd/yyyy").val(null);
    exp.find("#input_start_date_label-1").attr("id", "input_start_date_label-" + count).attr("for", "input_start_date-" + count);
    exp.find("#input_end_date-1").attr("id", "input_end_date-" + count)
        .attr("placeholder", "mm/dd/yyyy").val(null);
    exp.find("#input_end_date_label-1").attr("id", "input_end_date_label-" + count).attr("for", "input_end_date_label-" + count);
    exp.find("#input_experience_description-1").attr("id", "input_experience_description-" + count)
        .attr("placeholder","როლი თანამდებიობაზე და ზოგადი აღწერა").val(null);
    exp.find("#input_experience_description_label-1").attr("id", "input_experience_description_label-" + count)
        .attr("for", "input_experience_description-" + count);
    $("#experience").append(exp);
}

function add_new_resume_experience(resume_count) {
    let resume = $("#resume_experience_div").clone();
    var res = resume.find("#resume_experience_div-1").attr("id", "resume_experience_div-" + resume_count);
    res.find("#resume_experience-1")
        .attr("id", "resume_experience-" + resume_count)
        .val("");
    res.find("#resume_position-1")
        .attr("id", "resume_position-" + resume_count);
    res.find("#resume_employer-1")
        .attr("id", "resume_employer-" + resume_count);
    res.find("#resume_experience_date_start-1")
        .attr("id", "resume_experience_date_start-" + resume_count);
    res.find("#resume_experience_date_end-1")
        .attr("id", "resume_experience_date_end-" + resume_count);
    res.find("#resume_experience_description-1")
        .attr("id", "resume_experience_description-" + resume_count);
    $("#resume_experience_div").append(res);
    if(resume_count > 1) {
        document.getElementById("resume_experience-" + resume_count).style.visibility = 'hidden';
        document.getElementById("resume_position-" + resume_count).innerHTML = '';
        document.getElementById("resume_employer-" + resume_count).innerHTML = '';
        document.getElementById("resume_experience_date_start-" + resume_count).innerHTML = '';
        document.getElementById("resume_experience_date_start-" + resume_count).innerHTML = '';
        document.getElementById("resume_experience_date_end-" + resume_count).innerHTML = '';
        document.getElementById("resume_experience_description-" + resume_count).innerHTML = '';
    }
}