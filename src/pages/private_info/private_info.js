function fill_input_fname_text() {
    var x = document.getElementById("fname").value;
    document.getElementById("resume_name").innerHTML = x;
    sessionStorage.setItem("fname", x);
    if(validate_fname_lname_text(x) === 0) {
        document.getElementById("fname").style.border = '1px solid #98E37E';
        document.getElementById("name_ok").style.visibility = 'visible';
        document.getElementById("name_bad").style.visibility = 'hidden';
    } else {
        document.getElementById("fname").style.border = '1px solid #EF5050';
        document.getElementById("name_bad").style.visibility = 'visible';
        document.getElementById("name_ok").style.visibility = 'hidden';
    }
}

function fill_input_lname_text() {
    var x = document.getElementById("lname").value;
    document.getElementById("resume_lname").innerHTML = x;
    sessionStorage.setItem("lname",x);
    if(validate_fname_lname_text(x) === 0) {
        document.getElementById("lname").style.border = '1px solid #98E37E';
        document.getElementById("lname_ok").style.visibility = 'visible';
        document.getElementById("lname_bad").style.visibility = 'hidden';
    } else {
        document.getElementById("lname").style.border = '1px solid #EF5050';
        document.getElementById("lname_bad").style.visibility = 'visible';
        document.getElementById("lname_ok").style.visibility = 'hidden';
    }
}

function validate_fname_lname_text(name_text){
    if (name_text.length < 2) {
        return -1;
    } else {
        for(let i = 0; i < name_text.length; i++) {
            if (4304 > name_text.charCodeAt(i) || name_text.charCodeAt(i) > 4336) {
                return -2;
            }
        }
    }
    return 0;
}


function fill_input_about_me_text() {
    var x = document.getElementById("about_myself").value;
    document.getElementById("resume_about_me").innerHTML = x;
    var txt = document.getElementById("resume_about_me_red");
    if (x === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    sessionStorage.setItem("about_myself",x);
}

function fill_input_email() {
    var x = document.getElementById("email").value;
    document.getElementById("resume_email").innerHTML = x;
    var img = document.getElementById("resume_at");
    if (x === '') {
        img.style.visibility = 'hidden';
    } else {
        img.style.visibility = 'visible';
    }
    sessionStorage.setItem("email", x);
    if (validate_email(x) === 0){
        document.getElementById("email").style.border = '1px solid #98E37E';
        document.getElementById("email_ok").style.visibility = 'visible';
        document.getElementById("email_bad").style.visibility = 'hidden';
    } else {
        document.getElementById("email").style.border = '1px solid #EF5050';
        document.getElementById("email_bad").style.visibility = 'visible';
        document.getElementById("email_ok").style.visibility = 'hidden';
    }
}

function validate_email(email_text){
    var required_mail = "@redberry.ge";
    if(email_text.length < required_mail.length) {
        return -1;
    } else if (email_text.substr(email_text.length - required_mail.length) !== required_mail) {
        return -2;
    }
    return 0;
}

function fill_input_phone_number() {
    var x = document.getElementById("phone_number").value;
    document.getElementById("resume_number").innerHTML = x;
    var img = document.getElementById("resume_phone_image");
    if (x === '') {
        img.style.visibility = 'hidden';
    } else {
        img.style.visibility = 'visible';
    }
    sessionStorage.setItem("phone_number", x);
    if (vlaidate_phone_number(x) === 0){
        document.getElementById("phone_number").style.border = '1px solid #98E37E';
        document.getElementById("phone_ok").style.visibility = 'visible';
        document.getElementById("phone_bad").style.visibility = 'hidden';
    } else {
        document.getElementById("phone_number").style.border = '1px solid #EF5050';
        document.getElementById("phone_bad").style.visibility = 'visible';
        document.getElementById("phone_ok").style.visibility = 'hidden';
    }
}

function vlaidate_phone_number(phone_number){
    if(phone_number.length < 13) {
        return -1;
    } else if (phone_number.length < 5){
        return -2;
    } else if (phone_number.substr(0, 5) !== '+9955') {
        return -3
    }
    return 0;
}

function go_to_experience_page() {
    window.location.href = '../experience_page/experience_page.html';
}

function go_to_start_page() {
    sessionStorage.clear();
    window.location.href = '../../../index.html';
}

function get_session_data_private_info() {
    $(window).on('load', function(){
        const fname = sessionStorage.getItem("fname");
       if(fname !== null && fname !== '') {
           console.log(fname);
           $("#fname").val(fname);
       }
       const lname = sessionStorage.getItem("lname");
       if (lname !== null && lname !== '') {
           $("#lname").val(lname);
       }
       const about = sessionStorage.getItem("about_myself");
       if(about !== null && about !== '') {
           $("#about_myself").val(about);
       }
       const email = sessionStorage.getItem("email");
       if (email !== null && email !== '') {
           console.log(email);
           $("#email").val(email);
       }
       const number = sessionStorage.getItem("phone_number");
       if(number !== null && number !== '') {
           $("#phone_number").val(number);
       }
    });
}
var load_photo = function(event) {
    var image = document.getElementById('output');
    console.log("img" + event.target.files[0]);
    image.src=URL.createObjectURL(event.target.files[0]);
    console.log("img 2")
    sessionStorage.setItem("img_src", getBase64Image(image));
}
function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var dataURL = canvas.toDataURL("image/jpg");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}
