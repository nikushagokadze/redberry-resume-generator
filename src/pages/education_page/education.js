function fill_input_institute(id) {
    var edu_position = id.split("-")[1];
    var x = document.getElementById(id).value;
    document.getElementById("resume_institute-" + edu_position).innerHTML = x;
    var txt = document.getElementById("resume_education-" + edu_position);
    var y = document.getElementById("input_degree-" + edu_position).value;
    var z = document.getElementById('input_education_description-' + edu_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(edu_position, x, y, z);
    sessionStorage.setItem("input_institute-" + edu_position, x);
    if (validate_input(x) === 0){
        document.getElementById(id).style.border = '1px solid #98E37E';
        document.getElementById("institute_ok").style.visibility = 'visible';
        document.getElementById("institute_bad").style.visibility = 'hidden';
    } else {
        document.getElementById(id).style.border = '1px solid #EF5050';
        document.getElementById("institute_ok").style.visibility = 'hidden';
        document.getElementById("institute_bad").style.visibility = 'visible';
    }
}

function validate_input(text){
    if (text.length < 2){
        return -1;
    }
    return 0;
}
function fill_input_degree(id) {
    var edu_position = id.split("-")[1];
    var x = document.getElementById("input_degree-" + edu_position).value;
    document.getElementById("resume_degree-" + edu_position).innerHTML = x;
    var txt = document.getElementById("resume_education-" + edu_position);
    var y = document.getElementById("input_degree-"+ edu_position).value;
    var z = document.getElementById('input_education_description-' + edu_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(edu_position, x, y, z);
    sessionStorage.setItem("input_degree-1-" + edu_position, x);
}
function fill_input_education_description(id) {
    var edu_position = id.split("-")[1];
    var x = document.getElementById(id).value;
    document.getElementById("resume_education_description-" + edu_position).innerHTML = x;
    var txt = document.getElementById("resume_education-" + edu_position);
    var y = document.getElementById("input_degree-" + edu_position).value;
    var z = document.getElementById('input_education_description-' + edu_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(edu_position, x, y, z);
    sessionStorage.setItem("input_education_description-" + edu_position, x);
}

function fill_education_date(id) {
    var edu_position = id.split("-")[1];
    var x = document.getElementById(id).value;
    document.getElementById("resume_education_date-" + edu_position).innerHTML = x;
    var txt = document.getElementById("resume_education-" + edu_position);
    var y = document.getElementById("input_degree-" + edu_position).value;
    var z = document.getElementById('input_education_description-' + edu_position).value;
    if(x === '' && y === '' && z === '') {
        txt.style.visibility = 'hidden';
    } else {
        txt.style.visibility = 'visible';
    }
    check_filled_inputs_for_storage(edu_position, x, y, z);
    sessionStorage.setItem("input_end_date_edu-" + edu_position, x);
}

function check_filled_inputs_for_storage(position, x, y ,z) {
    console.log("position" + position);
    console.log("x " + x);
    console.log("y " + y);
    console.log("z " + z);
    if(position > 1 && (x !== '' || y !== '' || z !== '')) {
        sessionStorage.setItem("education-" + position, x);
    } else if (position > 1 && x === '' && y === '' && z === '' && sessionStorage.getItem("education-" + position) != null) {
        sessionStorage.removeItem("education-" + position);
    }
}

function go_to_experience_page() {
    window.location.href = "../experience_page/experience_page.html";
}

function load_resume_edu() {
    $(function () {
        $("#resume").load("/src/pages/resume/resume.html");
    })
}

function load_new_education() {
    $(function () {
        $("#button_more_institute").click(function () {
            var education = $("#education").clone();
            var count = 1;
            education.children().each(function () {
                    count++;
                }
            );
            add_new_education(count);

            var resume = $("#resume_education_div").clone();
            var resume_count = 1;
            resume.children().each(function () {
                resume_count++;
            });
            add_new_resume_education(resume_count);
        });
    });
}

function get_session_data_edu() {
    $(window).on('load', function(){
        let count = 2;
        while (sessionStorage.getItem("education-" + count) !== null) {
            console.log(sessionStorage.getItem("education-" + count));
            add_new_education(count);
            add_new_resume_education(count);
            count++;
        }

        $("#education").children().each(function () {
            const id = $(this).attr("id").split("-")[1];

            let input_institute = sessionStorage.getItem("input_institute-" + id);
            if (input_institute !== null && input_institute !== '') {
                $("#input_institute-" + id).val(input_institute);
            }

            let input_degree = sessionStorage.getItem("input_degree-1-" + id);
            if (input_degree !== null && input_degree !== '') {
                $("#input_degree-" + id).val(input_degree);
            }

            let input_edu_descr = sessionStorage.getItem("input_education_description-" + id);
            if(input_edu_descr !== null && input_edu_descr !=='') {
                $("#input_education_description-" + id).val(input_edu_descr);
            }

            let input_end_date_edu = sessionStorage.getItem("input_end_date_edu-" + id);
            if(input_end_date_edu !== null && input_end_date_edu !== '') {
                $("#input_end_date_edu-" + id).val(input_end_date_edu);
            }
        });
    });
}

function add_new_education(count) {
    let education = $("#education").clone();
    var edu = education.find("#education-1").attr("id", "education-" + count);
    edu.find("#input_institute-1").attr("id", "input_institute-" + count)
        .attr("placeholder", "სასწავლებელი").val(null);
    edu.find("#input_institute_label-1").attr("id", "input_institute_label-" + count).attr("for", "input_institute-" + count);
    edu.find("#input_degree-1").attr("id", "input_degree-" + count);
    edu.find("#input_degree_label-1").attr("id", "input_degree_label-" + count).attr("for", "input_degree-" + count);
    edu.find("#input_end_date_edu-1").attr("id", "input_end_date-" + count)
        .attr("placeholder", "mm/dd/yyyy").val(null);
    edu.find("#input_end_date_label-1").attr("id", "input_end_date_label-" + count).attr("for", "input_end_date_label-" + count);
    edu.find("#input_education_description-1").attr("id", "input_education_description-" + count)
        .attr("placeholder", "განათლების აღწერა").val(null);
    edu.find("#input_education_description_label-1").attr("id", "input_education_description_label-" + count)
        .attr("for", "input_education_description-" + count);
    $("#education").append(edu);
}

function add_new_resume_education(resume_count) {
    let resume = $("#resume_education_div").clone();
    var res = resume.find("#resume_education_div-1").attr("id", "resume_education_div-" + resume_count);
    res.find("#resume_education-1")
        .attr("id", "resume_education-" + resume_count)
    res.find("#resume_institute-1")
        .attr("id", "resume_institute-" + resume_count);
    res.find("#resume_education_description-1")
        .attr("id", "resume_education_description-" + resume_count);
    res.find("#resume_education_date-1")
        .attr("id", "resume_education_date-" + resume_count);
    res.find("#resume_degree-1")
        .attr("id", "resume_degree-" + resume_count);
    $("#resume_education_div").append(res);
    if(resume_count > 1) {
        document.getElementById("resume_education-" + resume_count).style.visibility = 'hidden';
        document.getElementById("resume_institute-" + resume_count).innerHTML = '';
        document.getElementById("resume_employer-" + resume_count).innerHTML = '';
        document.getElementById("resume_education_description-" + resume_count).innerHTML = '';
        document.getElementById("resume_education_date-" + resume_count).innerHTML = '';
        document.getElementById("resume_education_date-" + resume_count).innerHTML = '';
    }
}

function get_education_titles(id) {
    const request = new XMLHttpRequest();
    request.open("GET", "https://resume.redberryinternship.ge/api/degrees");
    request.send();
    let response;
    request.onload = () => {
        if (request.readyState == 4 && request.status == 200) {
            response = JSON.parse(request.response);
            for (const res in response) {
                $("#" + id).append("<option class=\"education_degree_option_box education_degree_option_txt\" value=" + response[res].title  + "\">" + response[res].title +"</option>");
            }
        }
    };
}

function submit_document() {
    fetch("https://resume.redberryinternship.ge/api/cvs", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "name": sessionStorage.getItem("fname"),
            "surname": sessionStorage.getItem("lname"),
            "email": sessionStorage.getItem("email"),
            "phone_number": sessionStorage.getItem("phone_number"),
            "experiences": [
                {
                    "position": sessionStorage.getItem("input_position-1"),
                    "employer": sessionStorage.getItem("input_employer-1"),
                    "start_date": sessionStorage.getItem("input_start_date-1"),
                    "due_date": sessionStorage.getItem("input_end_date-1"),
                    "description": sessionStorage.getItem("input_experience_description")
                }
            ],
            "educations": [
                {
                    "institute": sessionStorage.getItem("input_institute-1"),
                    "degree_id": 7,
                    "due_date": sessionStorage.getItem("input_end_date_edu-1"),
                    "description": sessionStorage.getItem("input_education_description-1")
                }
            ],
            "image": sessionStorage.getItem("img_src"),
            "about_me": sessionStorage.getItem("about_myself")})
    })
        .then(response => response.json())
        .then(response => sessionStorage.setItem("response", JSON.stringify(response)))
        .then(response =>  window.location.href = '../last_page/last.html');
}